import {Link} from 'react-router-dom';
const Navbar = () => {
    return (  
        <div className="navbar">
            <h2>To Do App</h2>
            <div className="links">
                <Link to="/">Home</Link>
                <Link to="/DoneTasks">Done Tasks</Link>
                <Link to="/CanceledTasks">Canceled Tasks</Link>
                <Link to="/About">About</Link>
            </div>
        </div>
    );
}
 
export default Navbar;