import TodoContext from './context/TodoContext';
import { useContext} from "react";
import Todo from './Todo';

const DoneTodo = (props) => {
    const {doneTodos,setDoneTodos} = useContext(TodoContext);

    const removeTodoitem = (id,parent) => {
       if (parent ==='doneTodos'){
            const renewedArr = [...doneTodos].filter(todo => todo.id !== id);
            setDoneTodos(renewedArr);
        } 
    };

    // const [DoneTasks, setDoneTasks] = useState('Go foarm');
    return ( 
        <div>
            <h1>Done Tasks</h1>
            <div >
                <Todo 
                    parent="doneTodos"
                    todos={doneTodos} 
                    removeTodo ={removeTodoitem}
                      >
                </Todo>
            </div>
        </div>
     );
}
 
export default DoneTodo;