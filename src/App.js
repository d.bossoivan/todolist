import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.css';
import TodoContextProvider from './context/TodoContextProvider';
import Todolist from './TodoList';
import DoneTasks from './DoneTodo';
import CancelTodo from './CanceledTodo';
import Navbar from './Navbar';
import About from './About';

function App() {
  return (
    <TodoContextProvider>
    <Router>
      <div >
        <Navbar/>
        <div>
          <Switch>
            <Route  exact path="/">
              <Todolist/>
            </Route>

            <Route path="/DoneTasks">
              <DoneTasks/>
            </Route>
            <Route path="/CanceledTasks">
              <CancelTodo/>
            </Route>
            <Route path="/About">
              <About/>
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
    </TodoContextProvider>
    
  );
}

export default App;
