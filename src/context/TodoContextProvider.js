import TodoContext from './TodoContext';
import {useState} from 'react';

const TodoContextProvider = ({children}) => {

    const [undoneTodos, setUnDoneTodos] = useState([
        {id:1 ,text:'Go to heaven'},
        {id:2, text: 'Eat some food'}
    ]);
    const[ doneTodos, setDoneTodos] = useState([
        {id:3, text :'enter level 3'}
    ]);
    const [canceledTodos, setCanceledTodos] = useState([
        {id:9, text:'this task has been canceled'}
    ]);

    return ( 
        <TodoContext.Provider value = {{
            undoneTodos:undoneTodos,
            setUnDoneTodos:setUnDoneTodos,
            doneTodos:doneTodos,
            setDoneTodos:setDoneTodos,
            canceledTodos:canceledTodos,
            setCanceledTodos: setCanceledTodos
            }}>
            {children}
        </TodoContext.Provider>
     );
}
 
export default TodoContextProvider;