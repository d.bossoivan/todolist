import React, {useState} from 'react'
import TodoForm from './TodoForm';
import {TiEdit} from 'react-icons/ti'
import {RiDeleteBin2Line} from 'react-icons/ri'
import {TiTick} from 'react-icons/ti'


const Todo = (props) => {
    const [edit, setEdit] = useState({
        id:null,
        value:''
    });

    const submitUpdate = value => {
        props.updateTodo(edit.id, value)
        setEdit({
            id:null,
            value: ''
        });

    }
    if (edit.id) {
        return <TodoForm edit ={edit} onSubmit ={submitUpdate} />;
    }

    return props.todos.map((todo,index) => (
        <div className='todo-row' key={index}>
                <div key={todo.id}>
                    {todo.text}
                </div>
                <div className="icons">
                   { props.parent === 'undoneTodos' && <TiEdit
                    onClick={() => setEdit({id:todo.id, value: todo.text})}
                    className="edit-icon"
                    />}
                    {props.parent === 'undoneTodos' && <TiTick
                    onClick={() => props.addDoneTodo(todo.id)}
                    />}
                    <RiDeleteBin2Line
                    onClick={() => props.removeTodo(todo.id, props.parent)}
                    className="delete-icon"
                    />  
                </div>
        </div>
    ));
}
export default Todo;