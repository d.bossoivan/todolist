import TodoContext from './context/TodoContext';
import Todo from './Todo';
import {useContext} from 'react';


const CancelTodo = () => {

    const {canceledTodos,setCanceledTodos} = useContext(TodoContext);

    const removeTodoitem = (id,parent) => {
       if (parent ==='canceledTodos'){
            const renewedArr = [...canceledTodos].filter(todo => todo.id !== id);
            setCanceledTodos(renewedArr);
        }  
    };

    return ( 
        <div>
            <h1>Canceled Tasks</h1>
            <Todo parent ='canceledTodos'
                todos ={canceledTodos}  
                removeTodo ={removeTodoitem}
            >
            </Todo>
        </div>
     );
}
 
export default CancelTodo;