import React, {useState, useEffect, useRef} from 'react'

const TodoForm = (props) => {
    const [input, setInput] = useState('Ex: Lire ma Bible');

    const inputRef = useRef(null)
    useEffect(() => {
        inputRef.current.focus()
    })
    const handleChange = (e) =>{
        setInput(e.target.value);
    };
    const handleSubmit = (e) => {
        e.preventDefault();//prevents the page from refreshing on click
        props.onSubmit({
            id:Math.floor(Math.random()*1000),
            text:input
        });
        setInput('');
    };
    return (
        <div>
            <form className="todo-form" onSubmit={handleSubmit}>
                <input type="text" 
                 placeholder="Ex: Buy some pens"
                 value={input} name='text' 
                 className='todo-input'
                 onChange={handleChange}
                  ref={inputRef} // force cursor on textEdit after refresh
                  />
                <button className="todo-button"> Add the task</button>
            </form>
        </div>
      );
}
 
export default TodoForm;