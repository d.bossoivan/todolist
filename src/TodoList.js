import TodoContext from './context/TodoContext';
import React, {useContext} from 'react';
import Todo from './Todo';
import TodoForm from './TodoForm';

const Todolist = () => {

    const {undoneTodos,setUnDoneTodos,doneTodos,setDoneTodos,canceledTodos,setCanceledTodos} = useContext(TodoContext);

    const addTodo = (todo) => {
        const newTodos = [todo, ...undoneTodos];
        setUnDoneTodos(newTodos);
    };
    const updateTodo = (todoId, newValue) => {
        setUnDoneTodos(prev => prev.map(item => (item.id === todoId? newValue : item))
        );
    }
    const removeTodoitem = (id,parent) => {
        if (parent === 'undoneTodos'){
            const renewedArr = [...undoneTodos].filter(todo => todo.id !== id);
            const newArr =  [...undoneTodos].filter(todo => todo.id == id);
            setUnDoneTodos(renewedArr);
            const newDoneArr =[...newArr, ...canceledTodos];
            setCanceledTodos(newDoneArr);
        } 
    };
    
    const addDoneTodo = (id) => {
        const renewedArr = [...undoneTodos].filter(todo => todo.id !== id);
        const newArr =  [...undoneTodos].filter(todo => todo.id == id);
        setUnDoneTodos(renewedArr);
        const newDoneArr =[...newArr, ...doneTodos];
        setDoneTodos(newDoneArr);
        };

    return ( 
        <div>
            <h1>Your Todo Planning</h1>
            <TodoForm onSubmit={addTodo}/>
            <h1>List of Tasks</h1>
            <Todo 
                parent="undoneTodos"
                todos={undoneTodos}
                addDoneTodo = {addDoneTodo}
                removeTodo ={removeTodoitem} 
                updateTodo={updateTodo}
                 >
            </Todo>
        </div>
     );
};
 
export default Todolist;